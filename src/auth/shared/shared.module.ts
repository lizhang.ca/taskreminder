import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AuthFormComponent } from './component/auth-form/auth-form.component';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [AuthFormComponent]
})
export class SharedModule { }
